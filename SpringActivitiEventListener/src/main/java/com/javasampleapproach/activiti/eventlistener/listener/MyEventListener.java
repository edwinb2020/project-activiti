package com.javasampleapproach.activiti.eventlistener.listener;

//import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.event.ActivitiEntityEvent;
import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.task.IdentityLinkType;
import org.activiti.engine.task.Task;
//import org.activiti.rest.service.api.runtime.task.TaskActionRequest;
//import org.activiti.engine.task.TaskQuery;
//import org.activiti.rest.service.api.runtime.task.TaskIdentityLinkCollectionResource;
//import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class MyEventListener implements ActivitiEventListener {

	@Autowired
	RuntimeService runtimeService;

	@Autowired
	TaskService taskService; 

	@Override
	public void onEvent(ActivitiEvent event) {
		switch (event.getType()) {
		case PROCESS_STARTED:
			System.out.println("New Process Instance has been started.");
			break;

		case PROCESS_COMPLETED:

			String message = (String) event.getEngineServices().getRuntimeService().getVariable(event.getExecutionId(),
					"message");

			System.out.println("Process (started by \"" + message + "\") has been completed.");
			break;

		case VARIABLE_CREATED:
			System.out.println("New Variable was created.");
			System.out.println(">> All Variables in execution scope: "
					+ event.getEngineServices().getRuntimeService().getVariables(event.getExecutionId()));
			break;

		case TASK_ASSIGNED:
			System.out.println("Task has been assigned." );
			break;

		case TASK_CREATED:
			System.out.println("Task has been created.");
		//get variableName dari assignment UserTask
		//cek tipe data variable
		//jika tipe array, hapus current participant, add 1 per 1 participant berdasarkan data yg terdapat pada array
          
         Task task = (Task) ((ActivitiEntityEvent) event).getEntity();
		 String taskId = task.getId();
		 System.out.println("Task : " + taskId); 
		 
		 String assignName = task.getAssignee();
         System.out.println("Task Assing Name : " + assignName);

         task.setAssignee("Dave"); 
		  
		 String assignChangeName = task.getAssignee();
         System.out.println("Task Assing Name (Change) : " + assignChangeName);
		 
		 String arrNewGroup[] = new String[] {"David","Dave","Deva"};
		   
		  for (String assigName : arrNewGroup) {
			  System.out.println("Add Participant - On Task Assign : "+ assigName);
			  //taskService.addCandidateUser(taskId, assigName);
		  }
  			break;

		case TASK_COMPLETED:
			System.out.println("Task \""
					+ event.getEngineServices().getHistoryService().createHistoricTaskInstanceQuery()
							.orderByHistoricTaskInstanceEndTime().asc().list().get(0).getName()
					+ "\" has been completed.");
			break;

		default:
			break;
		}
	}

	@Override
	public boolean isFailOnException() {
		System.out.println("inside isFailOnException()");
		return false;
	}

}
